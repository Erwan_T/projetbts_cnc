//Procédure d'identification de la carte SD
File initialisation_SD() {
  //ouvre le dossier nommee "gcode.txt" dans la carte SD et informe l'utilisateur de la taille du dossier
  File dataFile = SD.open("gcode.txt");
  fileSize = dataFile.size();
  Serial.print("Le document fait ");
  Serial.print(fileSize);
  Serial.println(" octet.s");
  delay(1);
  return (dataFile);
}

//Procédure récupérant les données dans la carte SD
void lectSD (File SDFile) {
  if (SDFile.available()) {
    //Lit les donnees dans la carte µSD jusqu a la fin de la ligne
    while (donnee != '\n') {
      donnee = SDFile.read();
      txt[i] = donnee;
      i++;
    }
    //envoie les donnees recu a l ATmega328 qui contient GRBL
    Serial2.write(txt);
    //remise a zero des variables utilise lors de cette fonction
    donnee = '\0';
    memset(txt, '\0', sizeof(txt));
    i = 0;
    //Met le programme en mode attente d'une reponse du GRBL
    wait = 1;
  }
}

//Procédure mettant à jours la bande LED en fonction des données de la carte SD
void ledUpSD(File SDFile) {
  //Affiche une barre de progression via la bande Leds en fonction de la position de lecture actuelle sur le document
  fileSize = SDFile.size();
  filePos = SDFile.position();
  int ledsLimit = filePos * ledsPerStrip / fileSize;
  ledOn(blanc);
  for (int t = 0; t <= ledsLimit; t++) {
    leds.setPixelColor(t, vert);
  }
  leds.show();
}

//Procédure  mettant à jours l'ecran OLED en fonction des données de la carte SD
void oledUpSD() {
  //progression varie entre 0 et 1
  float progression = (float(filePos) / float(fileSize));
  //RAZ de l'ecran
  display.clearDisplay();
  //affichage de la barre de progression
  display.drawRect(1, display.height() - 10, display.width() - 1, 10, SSD1306_WHITE);
  //remplie la barre en fonction de progression (progression x largeur)
  display.fillRect(3, display.height() - 8, progression * display.width() - 5, 6, SSD1306_WHITE);
  //Mise de forme du texte
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(1, 0);
  //Affiche le texte envoye
  display.println(F("Commande:"));
  display.println(F(txt));
  //Si le bouton Start a été appuyé au préalable alors affiche Communication: ON sinon OFF
  display.print(F("Communication: "));
  if (runIt == 1) {
    display.println(F("ON"));
  } else {
    display.println(F("OFF"));
  }
  //Si le bouton Power a été appuyé au préalable alors affiche Power: ON sinon OFF
  display.print(F("Power: "));
  if (power == 1) {
    display.println(F("ON"));
  } else {
    display.println(F("OFF"));
  }
  //Met a jours l'affichage de l'ecran
  display.display();
  delay(1);
}

//Procédure vérifiant la fin de la transmission SD-GRBL
void checkEndSD(File SDFile) {
  //Si Power est mis a zero alors force l'arret du programme 
  if (power == 0) {
    SDFile.seek(SDFile.size());
  }
  //Si le programme est finit alors coupe les moteurs et le programme reprend depuis le début 
  if (SDFile.position() == SDFile.size()) {
    Serial2.write("M18"); //motor disable
    fin = 1;
  }
}
