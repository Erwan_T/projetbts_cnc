/*  Déclaration des broches
     MOSI - pin 11
     MISO - pin 12
     CLK - pin 13
     CS - 8

     BPStart - pin 4
     BPStop - pin 3
     BAU - pin 7
     BPPower - pin 6

     TX - pin 10
     RX - pin 9

     LED - pin 2

     Lock - pin 15

     SCL - pin 19
     SDA - pin 18

*/

#include <SPI.h>
#include <Wire.h>
#include <SD.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <avr/wdt.h>

//Declaration des boutons d'IHM, respectivement, bouton Start, Stop, Arret d'urgence et Power
#define BPStart 4
#define BPStop 3
#define BAU 7
#define BPPower 6

//declaration de la bande Leds contenants 14 Leds Neopixel RGB
#define LED 2
#define ledsPerStrip 14
Adafruit_NeoPixel leds = Adafruit_NeoPixel(ledsPerStrip, LED, NEO_RGB + NEO_KHZ800);


//declaration du verrou de securite
#define LockIN 15


//declaration de l'ecran OLED de 0.96" en liaison I2C a l'adresse 0x3D
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


//declaration de la carte µSD en liaison SPI
#define chipSD 8

//declaration des differentes variables
bool runIt, power, wait, fin = 0;
const byte buffVal = 32;
char txt[buffVal], txtGRBL[buffVal];
char donnee, donneeGRBL;
int i;
int duree, timer;
long filePos, fileSize;

//bibliotheque de couleur au format hexadecimal
int blanc = 0xFFFFFF;
int vert = 0xFF0000;
int rouge = 0x00FF00;
int bleu = 0x0000FF;

void setup() {
  //Le port serie 1 est utilise dans la communication entre la Teensy et le PC
  Serial.begin(115200);
  //Le port serie 2 est utilise dans la communication entre la Teensy et l'ATmega328 contenant GRBL
  Serial2.begin(115200);

  SD.begin(chipSD);

  pinMode(BPStart, INPUT_PULLUP);
  pinMode(BPStop, INPUT_PULLUP);
  pinMode(BAU, INPUT_PULLUP);
  pinMode(BPPower, INPUT_PULLUP);
  pinMode(LockIN,  INPUT_PULLUP);

  //attachInterrupt(digitalPinToInterrupt(BAU), urgence, LOW);
  //attachInterrupt(digitalPinToInterrupt(LockIN), urgence, LOW);

  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  leds.begin();
  ledOn(blanc);
  oledUpPC();
}

void loop() {
  //Boucle principale
  checkStart();
  File dataFile = initialisation_SD();
  delay(10000);
  if (dataFile == 1) {
    //Prog SD
    do {
      lectSD(dataFile);
      do {
        lectGRBL();
        do {
          readBP();
          ledUpSD(dataFile);
          oledUpSD();
        } while (runIt == 0);
      } while (wait == 1);
      checkEndSD(dataFile);
    } while (fin == 0);
    //Fin Prog SD
  } else {
    wdt_enable(WDTO_4S);
    ledOn(vert);
    //Prog PC
    do {
      lectPC();
      do {
        lectGRBL();
        do {
          readBP();
          oledUpPC();
          wdt_reset();
        } while (runIt == 0);
      } while (wait == 1);
      checkEndPC();
    } while (fin == 0);
    wdt_disable();
  }
  //Fin Prog PC
}
