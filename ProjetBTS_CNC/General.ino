//Procédure visant à regarder le lancement de la communication
void checkStart() {
  //Lit l'etat des differants boutons tant que ceux la ne sont pas active
  while ((runIt == 0) || (power == 0)) {
    oledUpPC();
    readBP();
  }
  //active les moteurs
  Serial2.write("M17"); //motor enable
}

//Procédure lisant l'état des boutons en surface
void readBP() {
  //lit L'etat des differants boutons
  if (runIt == 1) {
    if (digitalRead(BPStop) == 1) {
      runIt = 0;
    }
  } else if (runIt == 0) {
    if (digitalRead(BPStart) == 0) {
      runIt = 1;
    }
  }
  if (digitalRead(BPPower) == 0) {
    power = !power;
    Serial.println("power");
  }
  delay(100);
}

//Procédure allumant toutes les leds d'une couleur unique
void ledOn(int couleur) {
  //Allume tous les Leds de la couleur demande
  for (int t = 0; t < ledsPerStrip; t++) {
    leds.setPixelColor(t, couleur);
  }
  leds.show();
}

//Procédure récupérant les données envoyé par GRBL
void lectGRBL() {
  if (Serial2.available()) {
    //Lit les donnees renvoyer par GRBL jusqu a la fin de la ligne
    while (donneeGRBL != '\n') {
      donneeGRBL = Serial2.read();
      txtGRBL[i] = donneeGRBL;
      i++;
    }
    //envoie les donnees recu au PC
    Serial.write(txtGRBL);
    //remise a zero des variables utilise lors de cette fonction
    donneeGRBL = '\0';
    memset(txtGRBL, '\0', sizeof(txtGRBL));
    i = 0;
    //Desactive le mode attente
    wait = 0;
  }
}

//Procédure d'interruption d'urgence
void urgence() {
  //En cas d urgence reset le GRBL et la Teensy
  ledOn(rouge);
  Serial2.write(24); //commande ctrl^X qui reset GRBL
  wdt_enable(WDTO_15MS); //force le reset de la Teensy
  delay(10000);
}
