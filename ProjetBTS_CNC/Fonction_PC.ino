//Procédure mettant à jours l'affiche de l'ecran OLED
void oledUpPC() {
  //RAZ de l'ecran
  display.clearDisplay();
  //Mise en forme du texte
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(1, 0);
  //affiche le texte envoye
  display.println(F("Commande:"));
  display.println(F(txt));
  //Si le bouton Start a été appuyé au préalable alors affiche Communication: ON sinon OFF
  display.print(F("Communication: "));
  if (runIt == 1) {
    display.println(F("ON"));
  } else {
    display.println(F("OFF"));
  }
  //Si le bouton Power a été appuyé au préalable alors affiche Power: ON sinon OFF
  display.print(F("Power: "));
  if (power == 1) {
    display.println(F("ON"));
  } else {
    display.println(F("OFF"));
  }
  //Met a jours l'affichage de l'ecran
  display.display();
  delay(1);
}

//Procédure récupérant les données envoyer par le PC
void lectPC () {
  if (Serial.available()) {
    //Si le PC envoie des donnees a la Teensy alors elles les lit jusqu a la fin de la ligne
    while (donnee != '\n') {
      donnee = Serial.read();
      txt[i] = donnee;
      i++;
    }
    //envoie les donnees recu a l ATmega328 qui contient GRBL
    Serial2.write(txt);
    //remise a zero des variables utilise lors de cette fonction
    donnee = '\0';
    memset(txt, '\0', sizeof(txt));
    i = 0;
    //Met le programme en mode attente d'une reponse du GRBL
    wait = 1;
  }
}

//Procédure vérifiant la fin de la transmission PC-GRBL
void checkEndPC() {
  //Si Power est mis a zero alors force l'arret du programme
  if (power == 0) {
    memset(txt, '\0', sizeof(txt));
  }
  //Si le programme est finit alors coupe les moteurs et le programme reprend depuis le début 
  if (txt == '\0') {
    Serial2.write("M18"); //motor disable
    fin = 1;
  }
}
